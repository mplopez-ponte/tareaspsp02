package cena_filosofos;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author María Paz López Ponte
 */

public class Filosofo extends Thread {
    
    private final int id;
    private final Semaphore [] semaphore;
    private final int [][] palillo;
    private final int palillo_Izq;
    private final int palillo_Drch;
    
    //El constructor va a crear una instancia de filosofo con su ID, su semáforo
    //y sus palillos con cuyas posiciones en el array de los palillos de dos dimensiontes
    //coincidirá con el id en la primera dimensión, y con 0 si es el palillo izquierdo
    //o 1 si es el derecho, en la segunda dimensión.
    
    public Filosofo(int id, Semaphore [] semaphore, int[][] palillo) {
        
        this.id=id;
        this.semaphore=semaphore;
        this.palillo=palillo;
        palillo_Izq=palillo[id][0];
        palillo_Drch=palillo[id][1];
    }
    
    //Este método va a permitir al filosofo comer.
    protected void comer() {
        if(semaphore[palillo_Izq].tryAcquire()){ //se comprueba si el semáforo permite agarrar
            //el palillo izquierdo...
            System.out.println("El filósofo "+id+" está comiendo...");
            try {
                sleep(500); //Se va a dormir el hilo durante un segundo
            } catch (InterruptedException ex) { //vamos a capturar el posible error
                Logger.getLogger(Filosofo.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("El filósofo "+id+" ha terminado de comer. Deja libre los palillos...");
            semaphore[palillo_Izq].release();//se libera el palillo izq. para que otro filosofo pueda comer
            semaphore[palillo_Drch].release();//se libera el palillo derecho
        } else { //en el caso de no poder agarrar los palillos se indica
            System.out.println("El filósofo "+id+" está hambriento...");
        }
    }
    //Este método permite pensar al filosofo
    protected void pensar() {
        System.out.println("El filósofo "+id+" está pensando...");
        try {
            Filosofo.sleep(1000, 500); //Vamos a dormir el hilo 2 segundos.
            
        } catch (InterruptedException ex) { //Se captura un posible error
            Logger.getLogger(Filosofo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Sobreescribimos el método run() que ejecute de forma indefinida
    // a través de un bucle while el pensar y el comer de los hios que se han creados en el main
    // de la aplicación que corresponda.
    
    @Override
    public void run() {
        
        while(true) {
            pensar();
            comer();
        }
    }

	public int [][] getPalillo() {
		return palillo;
	}
}