/*
 * Esta aplicación va a crear 5 instancias de la clase filosofo, y que se van a 
 * gestionar con un objeto de la clase semaforo, para que así los filosofos puedan
 * agarrar los palillos sin generar ningún error, y puedan comer y pensar.
 */

package cena_filosofos;

import java.util.concurrent.Semaphore;

/**
 *
 * @author María Paz López Ponte
 */
public class Cena_Filosofos {

    final static int num_Filosofos=5;
    //Crearemos un array de dos dimensiones de los palillos. La primera dimensión
    //sirve para identificar los filosofos, la segunda es para poder identificar los palillos.
        
    final static int [][] palillo={ {0,4}, {1,0}, {2,1}, {3,2}, {4,3} };
    
    //Se gestiona el semáforo con los accesos
    private static Semaphore[] semaphore = new Semaphore[5];
    
    public static void main(String[] args) {
        //Se crean los permisos para el semáforo
        for(int i=0; i<num_Filosofos; i++) {
            semaphore[i]=new Semaphore(1);
        }
        
        //Crearemos los filosofos con su id, el semáforo y sus palillos correspondientes
        
        for(int id=0; id<num_Filosofos; id++) {
            new Filosofo(id, semaphore, palillo).start();
        }
    }
}