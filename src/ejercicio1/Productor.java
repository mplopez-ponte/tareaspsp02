package ejercicio1;

import java.util.Random;

public class Productor extends Thread {
	
	private Pila pila;
	private String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private char c;
	private Random random = new Random();
	
	
	public Productor (Pila pila, int cant, int ms){
		this.pila = pila;
	
	}
	
	public Productor (Pila p){
		this.pila = p;;
	}
	
	public void run (int i){
		
			c = alfabeto.charAt(random.nextInt(26));
			System.out.println("Depositado el carácter "+c+" en el buffer");
			pila.poner(c);
		
	}
}
