package ejercicio1;

public class Consumidor extends Thread {
	private Pila pila;

    public Consumidor (Pila pila){
		this.pila = pila;
	}

	public void run (int i){
	        char c;
			c = pila.sacar();
			
		   System.out.println("Recogido el carácter "+c+" en el buffer ");
		   }
}