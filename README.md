## Tarea para PSP02.

### Ejercicio 1

**Proponemos un ejercicio del tipo productor-consumidor que mediante un hilo productor almacene datos (15 caracteres) en un búfer compartido, de donde los debe recoger un hilo consumidor (consume 15 caracteres). La capacidad del búfer ahora es de 6 caracteres, de manera que el consumidor podrá estar cogiendo caracteres del búfer siempre que éste no esté vacío. El productor sólo podrá poner caracteres en el búfer, cuando esté vacío o haya espacio.**

Para poder realizar este ejercicio se ha creado una __clase Main__ (clase principal) que crea una nueva Pila para poder almacenar una letra del abecedario, y se crea un objeto de tipo Producto (genera de forma aleatoria una letra del abecedario) y también crearemos otro objeto de tipo Consumidor (recoge esa letra del buffer).

__Clase Pila__: En esta clase se almacena el carácter generado por el proceso productor y lo almacena en un array. Esta pila es el elemento que comparte, tanto el productor como el consumidor para que se produzca la comunicación entre ambos objetos.

__Clase Productor__: En la clase productor, se genera una letra de forma aleatoria entre todas las letras que conforman el alfabeto (26 letras). Esta generación aleatoria, se almacena en una variable.

__Clase Consumidor__: En la clase consumidor, se sincroniza con la pila y obtiene la letra generada por el proceso productor.

El resultado de ejecutar esto se muestra en la siguiente imagen:

![img1](https://bitbucket.org/mplopez-ponte/tareaspsp02/downloads/Resultado%20ejercicio1.jpg) 

### Ejercicio 2

**El problema es el siguiente: Cinco filósofos se sientan alrededor de una mesa y pasan su vida comiendo y pensando. Cada filósofo tiene un plato de arroz chino y un palillo a la izquierda de su plato. Cuando un filósofo quiere comer arroz, cogerá los dos palillos de cada lado del plato y comerá. El problema es el siguiente: establecer un ritual (algoritmo) que permita comer a los filósofos. El algoritmo debe satisfacer la exclusión mutua (dos filósofos no pueden emplear el mismo palillo a la vez), además de evitar el interbloqueo y la inanición.**

En este ejercicio, y con el uso de la clase semaphore, se consigue que todos los filósofos puedan comer, y por supuesto consiguiendo la exclusión mutua (para que dos filósofos no puedan utilizar el mismo palillo a la vez), además de poder evitar el interbloqueo y que algún filosofo llegue a un estado de inanición.

Para este ejercicio se usaron dos clases:
La clase __**Cena_Filosofos**__ será la clase principal, y por lo tanto dispondrá del método main en el cual se crea un array de dos dimensiones la cual la primera servirá para identificar a los filósofos, y la segunda dimensión para identificar los palillos.
La segunda clase es la clase __**Filosofo**__, la cual estableceremos la composición de la instancia de filosofo con su ID, su semáforo y sus palillos.
En esta clase, también se desarrollan los métodos comer, pensar y ya por último con el método run se ejecutan los hilos creados en el main.

El resultado de este ejercicio, se muestra en la siguiente imagen:
![img2](https://bitbucket.org/mplopez-ponte/tareaspsp02/downloads/Selecci%C3%B3n_001.jpg)
